def main() {
  a = [ 1, 2, 'aoeu', [ 3, 4 ] ]
  b = {| 'c' := 5, 'd' := graph |}
  append(b, a)

  c = [ 1, 2, 'aoeu', [ 3, 4 ] ]

  print( isEqual( a, c ) )

  d = {| 'c' := 5, 'd' := graph |}
  append(d, c)

  print( isEqual( a, c ) )
}

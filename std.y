def isEmpty( iterable ) {
  ret = true
  forKey( a,iterable ){
    return( false )
  }
  return( ret )
}

def size( iterable ) {
  ret = 0
  forKey( a,iterable ){
    ret = ret + 1
  }
  return( ret )
}

def enqueue( item,list ) {
  return( append( item, list ) )
}

def dequeue( list ){
  if( isEmpty( list ) ) {
    return( NULL )
  }
  else {
    return( remove( 0, list ) )
  }
}

def push( item, list ) {
  append( item, list )
}

def pop( list ){
  if( isEmpty( list ) ) {
    return( NULL )
  }
  else {
    return( remove( size( list ) - 1, list )  )
  }
}

def isNull( x ) {
  return( typeOf( x ) == -1 )
}

def isNum( x ){
  return( typeOf( x ) == 0 )
}

def isString( x ){
  return( typeOf( x ) == 1 )
}

def isList( x ) {
  return( typeOf( x ) == 2 )
}

def isMap( x ) {
  return( typeOf( x ) == 3 )
}

def isGraph( x ) {
  return( typeOf( x ) == 4 )
}

def isEqual( a, b ) {
  if( not ( typeOf( a ) == typeOf( b ) ) ) {
     return( false )
  }
  else {
    if( isNull( a )  or isNum( a ) or isString( a ) ) {
       return( a == b )
    }
    else {
      // slightly more complicated cases
      if( isList( a ) or isMap( a ) ) {
        if( size( a ) != size( b ) ) {
          return( false )
        }
        elemsEqual = 0
        forKey( elem, a ) {
          elemsEqual = elemsEqual + isEqual( a[elem], b[elem] )
        }
        // Return the equality
        return( elemsEqual == size( a ) )
      }
      else {
      // graph, only other type
        return( isEqual( v( a ), v( b ) ) and isEqual( e( a ), e( b ) ) )
      }
    }
  }
}

def deepCopy( a ) {
  if( isNull( a ) ) {
    return( NULL )
  }
  if( isNum( a ) ) {
    return( a )
  }
  if( isString( a ) ) {
    return( a )
  }
  if( isList( a ) ) {
    b = []
    forKeyValue( i, v, a ) {
      append( deepCopy( v ), b )
    }
    return( b )
  }
  if( isMap( a ) ) {
    b = {||}
    forKeyValue( k, v, a ) {
      b[ k ] = deepCopy( v )
    }
    return( b )
  }
  if( isGraph( a ) ) {
    b = graph
    forKeyValue( label, v, v( a ) ) {
      newV = addV( b, label )
      forKeyValue( k, attr, v ) {
        newV[ k ] = deepCopy( attr )
      }
    }
    forKeyValue( label, e, e( a ) ) {
      newE = addE( a, e.orig.label, e.dest.label )
      forKeyValue( k, attr, e ) {
        newE[ k ] = deepCopy( attr )
      }
    }
    return( b )
  }
}

def isIn( item, iterable ){
  // iterable should be a list or a map
  if( isList( iterable ) ) {
    forKeyValue( i, val, iterable ) {
      if( isEqual( val, item ) ) {
       return( true )
      }
    }
      return( false )
  }
  if( isMap( iterable ) ) {
    forKey( k, iterable ){
      if( isEqual( k, item ) ) {
        return( true )
      }
    }
    return( false )
  }
  return( false )
}

def adj( G, v ) {
  res = []
  if( isString( v ) ) {
    forKeyValue(label, edge, e(G)) {
      if (edge.orig.label == v) {
        append(edge.dest.label, res)
      }
    }
  }
  if( isMap( v ) ) {
    forKeyValue(label, edge, e(G)) {
      if (edge.orig.label == v.label) {
        append(edge.dest, res)
      }
    }
  }
  return( res )
}

def edgeLabel(u, v) {
  return( u.label + '~' + v.label )
}


(* Code generation: translate takes a semantically checked AST and
produces LLVM IR

LLVM tutorial: Make sure to read the OCaml version of the tutorial

http://llvm.org/docs/tutorial/index.html

Detailed documentation on the OCaml LLVM library:

http://llvm.moe/
http://llvm.moe/ocaml/

*)

module L = Llvm
module A = Ast

module StringMap = Map.Make(String);;

exception Uninitialized_var of string;;

module StringHash = Hashtbl.Make(struct
  type t = string
  let equal x y = x = y
  let hash = Hashtbl.hash end);;

let translate (functions) =
  let context = L.global_context () in
  let the_module = L.create_module context "YAGL"
  and i32_t = L.i32_type context
  and i8_t = L.i8_type context
  (* and i1_t = L.i1_type context *)
  and double_t = L.double_type context
  and void_t = L.void_type context
  and pointer_t = L.pointer_type
  and string_t = (L.array_type (L.i8_type context) 0)
  and struct_t = L.named_struct_type context "struct_t"
  and node_t = L.named_struct_type context "node_t"
  and graph_t = L.named_struct_type context "graph_t" in

  let zero = L.const_int (L.i32_type context) 0 and
      one = L.const_int (L.i32_type context) 1 and
      two = L.const_int (L.i32_type context) 2 and
      three = L.const_int (L.i32_type context) 3 and
      four = L.const_int (L.i32_type context) 4 and
      ten = L.const_int i32_t 10 and
      minus_one = L.const_int (L.i32_type context) (-1) in
  let num_type = L.const_int (L.i32_type context) 0 and
      str_type = L.const_int (L.i32_type context) 1 and
      lst_type = L.const_int (L.i32_type context) 2 and
      map_type = L.const_int (L.i32_type context) 3 and
      graph_type = L.const_int (L.i32_type context) 4 and
      null_type = L.const_int (L.i32_type context) (-1) in
  let f_zero = L.const_float double_t 0.0 and
      f_one  = L.const_float double_t 1.0 in

  (* Every data type in YAGL is represented with a single data type under the
     hood: a struct containing pointers to the possible YAGL types. No more than
     one of these pointers should be non-null at any point; the non-null type
     (if any) is the YAGL type that the struct actually represents. *)
  ignore (L.struct_set_body struct_t
    [| pointer_t double_t;  (* number type *)
       pointer_t string_t;  (* string type *)
       pointer_t node_t;  (* list type *)
       pointer_t node_t;  (* map type *)
       pointer_t graph_t  (* graph type *)
    |]
  false);

  (* Node in the style of a linked list node for representing lists, sets, and
     maps *)
  ignore (L.struct_set_body node_t
    [| pointer_t string_t;  (* Key for maps; null for lists *)
       pointer_t struct_t;  (* Data for this entry *)
       pointer_t node_t  (* Pointer to the next node *)
    |]
  false);

  (* Graph representation *)
  ignore (L.struct_set_body graph_t
    [| pointer_t node_t;  (* Vertices *)
       pointer_t node_t  (* Edges *)
    |]
  false);

  (* C's printf *)
  let printf_t = L.var_arg_function_type i32_t [| pointer_t i8_t |] in
  let printf_func = L.declare_function "printf" printf_t the_module in

  (* C's abort *)
  (* TODO: delete if this is not actually used anywhere *)
  let abort_t = L.var_arg_function_type void_t [||] in
  let abort_func = L.declare_function "abort" abort_t the_module in

  (* C's strcmp *)
  let strcmp_t = L.var_arg_function_type i32_t [| pointer_t i8_t; pointer_t i8_t |] in
  let strcmp_func = L.declare_function "strcmp" strcmp_t the_module in

  (* C's strcat *)
  let strcat_t = L.var_arg_function_type i32_t [| pointer_t i8_t; pointer_t i8_t |] in
  let strcat_func = L.declare_function "strcat" strcat_t the_module in

  (* C's strcpy *)
  let strcpy_t = L.var_arg_function_type i32_t [| pointer_t i8_t; pointer_t i8_t |] in
  let strcpy_func = L.declare_function "strcpy" strcpy_t the_module in

  (* C's strlen *)
  let strlen_t = L.var_arg_function_type i32_t [| pointer_t i8_t |] in
  let strlen_func = L.declare_function "strlen" strlen_t the_module in

  (* Define my own funciton *)
  let myfunc_t = L.var_arg_function_type double_t [| pointer_t double_t |] in
  let myfunc_func = L.define_function "myfunc" myfunc_t the_module in
  let myfunc_bb = L.entry_block myfunc_func in
  let myfunc_builder = L.builder_at_end context myfunc_bb in
      let a_ptr = L.param myfunc_func 0 in
      let b_ptr = L.param myfunc_func 1 in
      let a_real = (L.build_load a_ptr "a_load" myfunc_builder ) in
      let a_ret = L.build_fadd a_real f_one "myf_add" myfunc_builder in
      ignore( L.build_ret a_ret myfunc_builder );
   (*   ignore(L.position_at_end myfunc_bb myfunc_builder); *)


  (* As the default type of any variable in the IR is a struct, the type of any
     input to a function is a struct pointer. Every function returns a struct
     in the IR, except for the main function, which returns void. *)
  let function_decls =
    let rec list_of_n_struct_ptrs n =
      if n = 0 then []
      else pointer_t struct_t :: (list_of_n_struct_ptrs (n - 1)) in
    let array_of_n_struct_ptrs n = Array.of_list (list_of_n_struct_ptrs n) in

    let function_decl m fdecl =
      let name = fdecl.A.fname in
      let return_type =
        if name = "main" then void_t
        else (pointer_t struct_t) in
      StringMap.add name
        (L.define_function name
          (L.function_type
            return_type (array_of_n_struct_ptrs (List.length fdecl.A.formals)))
          the_module,
        fdecl) m in
    List.fold_left function_decl StringMap.empty functions in

  let build_function_body fdecl =
    let (the_function, _) = StringMap.find fdecl.A.fname function_decls in

    (* The main builder for the function body *)
    let builder = L.builder_at_end context (L.entry_block the_function) in

    (* Global strings for printing variables and error messages *)
    (* TODO: move out of build_function_body; it doesn't make sense here *)
    let int_format_str =
      L.build_global_stringptr "%d\n" "fmt_int" builder in
    let float_format_str =
      L.build_global_stringptr "%.9f\n" "fmt_float" builder in
    let string_format_str =
      L.build_global_stringptr "%s\n" "fmt_string" builder in
    let bad_index_str =
      L.build_global_stringptr "ERROR: Bad list index" "bad_index" builder in
    let diff_types_str =
      L.build_global_stringptr "ERROR: Incompatible types" "diff_types" builder in
    let invalid_print_str =
      L.build_global_stringptr "ERROR: Invalid print type" "print_types" builder in
    let type_check_format_str =
      L.build_global_stringptr ("\nERROR: Expected item of type:  %d  " ^
        "Found item of type:  %d\n{| 'NULL' := -1, 'Num' := 0, 'String' := 1," ^
        " 'List' := 2, 'Map' := 3, 'Graph' := 4 |}\n") "type_err_string" builder
      in
    let left_type_str =
      L.build_global_stringptr "ERROR: left item of type:" "non_lst" builder in
    let right_type_str =
      L.build_global_stringptr "ERROR: right item of type:" "non_lst" builder in
    let description_type_str =
      L.build_global_stringptr ("{| 'NULL' := -1, 'Num' := 0, 'String' := 1, " ^
      "'List' := 2, 'Map' := 3, 'Graph' := 4 |}") "description" builder in
    let here_str =
      L.build_global_stringptr "HERE" "here" builder in
    let null_str =
      L.build_global_stringptr "NULL" "NULL" builder in

    let print_here builder =
      ignore( L.build_call printf_func [| string_format_str; here_str |]
        "here" builder )
    in

    (* In YAGL, when a variable is defined anywhere in a function, it is
       accessible at any point in the control flow afterwards, even if it was
       defined in, say, an if-else block. To make this possible in LLVM, we
       create a basic block for mallocting new memory for variables, which gets
       run first. Any time we use build_malloc, we also use the builder
       rather than the main builder. *)
    let malloc_bb = L.append_block context "malloc_vars" the_function in
    let malloc_builder = L.builder_at_end context malloc_bb in
    ignore (L.build_br malloc_bb builder);

    (* The basic block for function statements, which gets run after the
       variable malloction basic block in the IR. The break from the malloction
       block to this block is set at the very end, after we are sure that no
       more variables can be defined. *)
    let stmts_bb = L.append_block context "stmts" the_function in
    L.position_at_end stmts_bb builder;

    (* TODO: this function was copied over and is still a bit mysterious.
       Investigate and either remove or write a better comment. *)
    let add_terminal builder f =
    match L.block_terminator (L.insertion_block builder) with
      Some _ -> ()
    | None -> ignore (f builder)
    in

    (* Helper functions to retrieve data pointers from a struct. *)
    let extract_struct_index s i builder =
      let struct_elem =
        L.build_in_bounds_gep s [| zero; i |] "struct_elem" builder in
      L.build_load struct_elem "elem_ptr" builder
    in
    let extract_num s builder = extract_struct_index s zero builder in
    let extract_string s builder = extract_struct_index s one builder in
    let extract_list s builder = extract_struct_index s two builder in
    let extract_map s builder = extract_struct_index s three builder in
    let extract_graph s builder = extract_struct_index s four builder in

    let extract_num_val s builder =
      L.build_load (extract_num s builder) "num_val" builder
    in

    let extract_list_val s builder =
      L.build_load (extract_list s builder) "num_val" builder
    in
    (* Helper functions to retrieve data pointers from a node *)
    let extract_node_string n builder = extract_struct_index n zero  builder in
    let extract_node_struct n builder = extract_struct_index n one   builder in
    let extract_node_node   n builder = extract_struct_index n two builder in

    let extract_graph_v g builder = extract_struct_index g zero builder in
    let extract_graph_e g builder = extract_struct_index g one builder in

    (* Helper function which takes a struct contianing a number and returns an
       int value 0 or 1 if it represents a true or false value *)

    let is_true s builder =
      let num_val = extract_num_val s builder in
      L.build_fcmp L.Fcmp.One num_val f_zero "is_true" builder
    in

    let valid_struct_index s builder =
      let is_num =
        L.build_is_not_null (extract_num s builder) "is_num" builder in
      let is_string =
        L.build_is_not_null (extract_string s builder) "is_string" builder in
      let is_list =
        L.build_is_not_null (extract_list s builder) "is_list" builder in
      let is_map =
        L.build_is_not_null (extract_map s builder) "is_map" builder in
      let is_graph =
        L.build_is_not_null (extract_graph s builder) "is_graph" builder in
      let result = L.build_malloc i32_t "result" builder in

      let merge_bb = L.append_block context "merge_valid_struct" the_function in

      let then_graph_bb = L.append_block context "then_graph"
        the_function in
      let then_graph_builder = L.builder_at_end context then_graph_bb in
      ignore (L.build_store four result then_graph_builder);
      ignore (L.build_br merge_bb then_graph_builder);

      let not_graph_bb = L.append_block context "not_graph" the_function in
      let not_graph_builder = L.builder_at_end context not_graph_bb in
      ignore (L.build_store minus_one result not_graph_builder);
      ignore (L.build_br merge_bb not_graph_builder);

      let then_map_bb = L.append_block context "then_map" the_function in
      let then_map_builder = L.builder_at_end context then_map_bb in
      ignore (L.build_store three result then_map_builder);
      ignore (L.build_br merge_bb then_map_builder);

      let not_map_bb = L.append_block context "not_map" the_function in
      let not_map_builder = L.builder_at_end context not_map_bb in
      ignore (L.build_cond_br is_graph then_graph_bb not_graph_bb not_map_builder);

      let then_list_bb = L.append_block context "then_list" the_function in
      let then_list_builder = L.builder_at_end context then_list_bb in
      ignore (L.build_store two result then_list_builder);
      ignore (L.build_br merge_bb then_list_builder);

      let not_list_bb = L.append_block context "not_list" the_function in
      let not_list_builder = L.builder_at_end context not_list_bb in
      ignore (L.build_cond_br is_map then_map_bb not_map_bb not_list_builder);

      let then_str_bb = L.append_block context "then_str" the_function in
      let then_str_builder = L.builder_at_end context then_str_bb in
      ignore (L.build_store one result then_str_builder);
      ignore (L.build_br merge_bb then_str_builder);

      let not_str_bb = L.append_block context "not_str" the_function in
      let not_str_builder = L.builder_at_end context not_str_bb in
      ignore (L.build_cond_br is_list then_list_bb not_list_bb not_str_builder);

      let then_num_bb = L.append_block context "then_num" the_function in
      let then_num_builder = L.builder_at_end context then_num_bb in
      ignore (L.build_store zero result then_num_builder);
      ignore (L.build_br merge_bb then_num_builder);

      let not_num_bb = L.append_block context "not_num" the_function in
      let not_num_builder = L.builder_at_end context not_num_bb in
      ignore (L.build_cond_br is_string then_str_bb not_str_bb not_num_builder);

      ignore (L.build_cond_br is_num then_num_bb not_num_bb builder);
      L.position_at_end merge_bb builder;
      L.build_load result "result" builder;
    in

    (* Helper function to build an empty struct containing null pointers. *)
    let build_empty_struct builder n =
      let empty_struct = L.build_malloc struct_t n builder in
      let struct_double = L.build_in_bounds_gep empty_struct [| zero; zero |]
            "struct_double" builder and
          null_double = L.const_pointer_null (pointer_t double_t) and
          struct_string = L.build_in_bounds_gep empty_struct [| zero; one |]
            "struct_string" builder and
          null_string = L.const_pointer_null (pointer_t string_t) and
          struct_list = L.build_in_bounds_gep empty_struct [| zero; two |]
            "struct_list" builder and
          null_list = L.const_pointer_null (pointer_t node_t) and
          struct_map = L.build_in_bounds_gep empty_struct [| zero; three |]
            "struct_map" builder and
          null_map = L.const_pointer_null (pointer_t node_t) and
          struct_graph = L.build_in_bounds_gep empty_struct [| zero; four |]
            "struct_graph" builder and
          null_graph = L.const_pointer_null (pointer_t graph_t) in
      ignore (L.build_store null_double struct_double builder);
      ignore (L.build_store null_string struct_string builder);
      ignore (L.build_store null_list struct_list builder);
      ignore (L.build_store null_map struct_map builder);
      ignore (L.build_store null_graph struct_graph builder);
      empty_struct
    in

    (* Helper function to build an empty node containing null pointers. *)
    let build_empty_node builder n =
      let empty_node = L.build_malloc node_t n builder in
      let node_str = L.build_in_bounds_gep empty_node [| zero; zero |]
            "node_str" builder and
          null_str = L.const_pointer_null (pointer_t string_t) and
          node_struct = L.build_in_bounds_gep empty_node [| zero; one |]
            "node_struct" builder and
          null_struct = L.const_pointer_null (pointer_t struct_t) and
          node_node = L.build_in_bounds_gep empty_node [| zero; two |]
            "node_node" builder and
          null_node = L.const_pointer_null (pointer_t node_t) in
      ignore (L.build_store null_str node_str builder);
      ignore (L.build_store null_struct node_struct builder);
      ignore (L.build_store null_node node_node builder);
      empty_node
    in

    (* Helper function to build an empty graph. *)
    let build_empty_graph builder n =
      let empty_graph = L.build_malloc graph_t n builder in
      let v = L.build_in_bounds_gep empty_graph [| zero; zero |]
            "v" builder and
          e = L.build_in_bounds_gep empty_graph [| zero; one |]
            "e" builder in
      ignore (L.build_store (build_empty_node builder "v_node") v builder);
      ignore (L.build_store (build_empty_node builder "e_node") e builder);
      empty_graph
    in

    (* Helper function to create a struct containing a number. *)
    let construct_str str_ptr builder =
      let str_struct = build_empty_struct builder "num_struct" in
      let struct_str = L.build_in_bounds_gep str_struct [| zero; one |]
        "struct_num" builder in
      ignore (L.build_store str_ptr struct_str builder);
      str_struct
    in

    (* Helper function to create a struct containing a number. *)
    let construct_num n builder =
      let num_struct = build_empty_struct builder "num_struct" in
      let struct_num = L.build_in_bounds_gep num_struct [| zero; zero |]
        "struct_num" builder in
      let num_ptr = L.build_malloc double_t "num_ptr" builder in
      ignore (L.build_store n num_ptr builder);
      ignore (L.build_store num_ptr struct_num builder);
      num_struct
    in

    (* Helper function to create a struct containing a graph. *)
    let construct_graph builder =
      let graph_struct = build_empty_struct builder "graph_struct" in
      let struct_graph = L.build_in_bounds_gep graph_struct [| zero; four |]
        "struct_graph" builder in
      ignore (L.build_store (build_empty_graph builder "graph") struct_graph
        builder);
      graph_struct
    in

    (* Initialize the local variables as the variables that were passed into the
       function. We store local variables in a hash map from the name to a
       struct pointer; the pointer is necessary to make sure that we handle
       variable reassignment properly. *)
    let local_vars =
      let add_formal m n p =
        L.set_value_name n p;
        StringHash.add m n p;
        m in
      List.fold_left2 add_formal (StringHash.create 0) fdecl.A.formals
        (Array.to_list (L.params the_function))
    in

    let print_struct builder s =
      let merge_bb = L.append_block context "merge_print" the_function in

      (* Build print num and print string blocks *)
      let print_num_bb = L.append_block context "print_num" the_function in
      let print_str_bb = L.append_block context "print_str" the_function in
      let print_null_bb = L.append_block context "print_null" the_function in

      let null_builder = L.builder_at_end context print_null_bb in
          ignore( L.build_call printf_func [| string_format_str; null_str |]
            "printf" null_builder );
          ignore( L.build_br merge_bb null_builder);

      let num_builder = L.builder_at_end context print_num_bb in
      let num = L.build_load ( extract_num s num_builder ) "num" num_builder in
        ignore( L.build_call printf_func [| float_format_str; num |] "printf"
          num_builder; );
        ignore (L.build_br merge_bb num_builder);

      let str_builder = L.builder_at_end context print_str_bb in
        ignore( L.build_call printf_func [| string_format_str;
          ( extract_string s str_builder ) |] "printf" str_builder; );
        ignore (L.build_br merge_bb str_builder);

      (* Build invalid print type block *)
      let invalid_type_bb = L.append_block context "invalid_print"
        the_function in
      let invalid_builder = L.builder_at_end context invalid_type_bb in
        ignore( L.build_call printf_func
          [| string_format_str; invalid_print_str |] "printf" invalid_builder );
        ignore( L.build_call abort_func [||] "" invalid_builder);
        ignore (L.build_br merge_bb invalid_builder);

      (* Determine what type this struct is *)
      let struct_type = valid_struct_index s builder in

      (* Construct switch statement between blocks *)
      let switch = L.build_switch struct_type invalid_type_bb 3 builder in
        L.add_case switch minus_one print_null_bb; (* Print null string *)
        L.add_case switch zero print_num_bb; (* Print like double *)
        L.add_case switch one print_str_bb; (* Print like string *)

      L.position_at_end merge_bb builder;
    in

    (* helper function to compare types of two structs returns type of structs
       if same else raises exception *)
    let type_comparison s1 s2 builder =
      let merge_bb = L.append_block context "merge_type_check" the_function in

      (* Determine if two structs can be operands to a binary operator *)
      let s1_index   = valid_struct_index s1 builder in
      let s2_index   = valid_struct_index s2 builder in
      let is_null_s1 = L.build_icmp L.Icmp.Eq s1_index minus_one "is_null"
        builder in
      let is_null_s2 = L.build_icmp L.Icmp.Eq s2_index minus_one "is_null"
        builder in
      let either_null= L.build_or is_null_s1 is_null_s2 "either_null" builder in
      let diff_type  = L.build_icmp L.Icmp.Ne s1_index s2_index "is_diff"
        builder in
      let is_invalid = L.build_or either_null diff_type "is_invalid" builder in

      (* If different types print they are different then throw *)
      let then_bb = L.append_block context "invalid_block" the_function in
      let then_builder = L.builder_at_end context then_bb in
        ignore( L.build_call printf_func [| string_format_str; diff_types_str |]
          "printf" then_builder );
        ignore( L.build_call printf_func [| string_format_str; left_type_str |]
          "expected_type" then_builder );
        ignore( L.build_call printf_func [| int_format_str; s1_index |]
          "expected_type" then_builder );
        ignore( L.build_call printf_func [| string_format_str; right_type_str |]
          "found_type" then_builder );
        ignore( L.build_call printf_func [| int_format_str; s2_index |]
          "found_type" then_builder );
        ignore( L.build_call printf_func [| string_format_str;
          description_type_str |] "found_type" then_builder );
        ignore (L.build_call abort_func [||] "" then_builder);
        ignore (L.build_br merge_bb then_builder);

      (* Else store the type in the return pointer *)
      let else_bb = L.append_block context "valid_block" the_function in
      let else_builder = L.builder_at_end context else_bb in
        ignore(L.build_br merge_bb else_builder);

      (* Construct the if statement *)
      ignore( L.build_cond_br is_invalid then_bb else_bb builder);
      L.position_at_end merge_bb builder;

      (* Load return value *)
      s1_index;
    in

    (* Checks if the type represented by an llvm number matches the correct type
       also represented by an llvm number. If not throws a useful error on the
       offending types *)
    let check_index_type this_type correct_type builder =

      let is_not_num = L.build_icmp L.Icmp.Ne this_type correct_type
        "is_not_correct" builder in
      let merge_bb = L.append_block context "merge_check" the_function in

      let then_bb = L.append_block context "type_check_throw" the_function in
      let then_builder = L.builder_at_end context then_bb in
        ignore( L.build_call printf_func [| type_check_format_str; correct_type;
          this_type |] "expected_type" then_builder );
        ignore( L.build_call abort_func [||] "" then_builder);
        ignore (L.build_br merge_bb then_builder);

      ignore( L.build_cond_br is_not_num then_bb merge_bb builder);
      L.position_at_end merge_bb builder;
    in
    (* Checks if struct is of a specific type. Also returns the struct type *)
    let check_struct_type s correct_type builder =
       let this_type = valid_struct_index s builder in
       ignore( check_index_type this_type correct_type builder );
       this_type;
    in

    (* Takes two structs; returns a struct with 1. or 0. indicating equality *)
    let is_equal s1 s2 builder =

      let s1_type = valid_struct_index s1 builder and (* s1_type is int *)
          s2_type = valid_struct_index s2 builder in (* s2_type is int *)
      let is_diff_type = L.build_intcast
        ( L.build_icmp L.Icmp.Ne s1_type s2_type "is_diff" builder) i32_t
        "cast_int" builder in (* is_diff_type is unsigned int *)
      (* ten_diff is type int *)
      let ten_diff = L.build_mul is_diff_type ten "mul" builder in
      (* this way if s1 and s2 are the same type then match is s1_type and if
         they are diff types switch_match is s1_type + 10 *)
      let switch_match = L.build_add ten_diff s1_type "add" builder in
      let ret_ptr = L.build_malloc (pointer_t struct_t) "ret_ptr" builder in

      let merge_bb = L.append_block context "merge_equal" the_function in

      (* Default Not Equal Block *)
      let not_equal_bb = L.append_block context "not_equal" the_function in
      let ne_builder = L.builder_at_end context not_equal_bb in
        ignore(L.build_store ( construct_num f_zero ne_builder ) ret_ptr
          ne_builder);
        ignore (L.build_br merge_bb ne_builder);

      (* Null Equality Block - Note: all nulls are equal so always return true *)
      let null_bb = L.append_block context "null_eq" the_function in
      let null_builder = L.builder_at_end context null_bb in
        ignore(L.build_store ( construct_num f_one null_builder ) ret_ptr
          null_builder);
        ignore (L.build_br merge_bb null_builder);

      (* Number Equality Block *)
      let double_bb = L.append_block context "double_eq" the_function in
      let double_builder = L.builder_at_end context double_bb in
        let num1 = extract_num_val s1 double_builder and
            num2 = extract_num_val s2 double_builder in
        let outcome = L.build_fcmp L.Fcmp.Oeq num1 num2 "eq" double_builder in
        let f_outcome = L.build_uitofp outcome double_t "fp" double_builder in
        ignore(L.build_store (construct_num f_outcome double_builder) ret_ptr
          double_builder);
        ignore (L.build_br merge_bb double_builder);

      (* String Equality Block *)
      let string_bb = L.append_block context "string_eq" the_function in
      let string_builder = L.builder_at_end context string_bb in
        let str1 = extract_string s1 string_builder and
            str2 = extract_string s2 string_builder in
        (* Get pointers to first char in either string  *)
        let char_ptr1 = L.build_in_bounds_gep str1 [| zero; zero |] "char_ptr"
              string_builder and
            char_ptr2 = L.build_in_bounds_gep str2 [| zero; zero |] "char_ptr"
              string_builder in
        let strcmp_val = L.build_call strcmp_func [| char_ptr1; char_ptr2 |]
          "str_eq" string_builder in (* Returns 0 if equal so need to flip *)
        let outcome = L.build_icmp L.Icmp.Eq strcmp_val zero "str_flip"
          string_builder in
        let f_outcome = L.build_uitofp outcome double_t "fp" string_builder in
        ignore( L.build_store (construct_num f_outcome string_builder) ret_ptr
          string_builder);
        ignore (L.build_br merge_bb string_builder);

      let switch = L.build_switch switch_match not_equal_bb 3 builder in
        L.add_case switch minus_one null_bb;   (* Go to Null Equality   Block *)
        L.add_case switch zero      double_bb; (* Go to Number Equality Block *)
        L.add_case switch one       string_bb; (* Go to String Equality Block *)

      L.position_at_end merge_bb builder;
      L.build_load ret_ptr "data" builder;
    in

    let list_append l s builder =
      let node_ptr = L.build_malloc (pointer_t node_t) "node_ptr"
        builder in
      ignore (L.build_store (extract_list l builder) node_ptr builder);

      let pred_bb = L.append_block context "for_each_while" the_function in
      ignore (L.build_br pred_bb builder);

      let body_bb = L.append_block context "list_iter_body" the_function in
      let body_builder = L.builder_at_end context body_bb in
        let node = L.build_load node_ptr "node" body_builder in
        let next_node = extract_node_node node body_builder in
        ignore (L.build_store next_node node_ptr body_builder);
      ignore (L.build_br pred_bb body_builder);

      let pred_builder = L.builder_at_end context pred_bb in

      let node = L.build_load node_ptr "node_ptr" pred_builder in
      let node_struct = extract_node_struct node pred_builder in
      let bool_val = L.build_is_not_null node_struct "bool_val" pred_builder in

      let merge_bb = L.append_block context "merge_for_each" the_function in
      ignore (L.build_cond_br bool_val body_bb merge_bb pred_builder);
      L.position_at_end merge_bb builder;

      let node_struct_ptr = L.build_in_bounds_gep node [| zero; one |]
        "node_struct_ptr" builder in
      let node_node = L.build_in_bounds_gep node [| zero; two |]
        "node_node" builder in
      let next_node = (build_empty_node builder "next_node") in
      ignore (L.build_store s node_struct_ptr builder);
      ignore (L.build_store next_node node_node builder);
    in

    (* Given a pointer to the first node of a list, and a pointer to an integer
       index i, iterate over the list and retrieve the ith element. *)
    let list_iter_node builder node_ptr i_ptr =
      let pred_bb = L.append_block context "list_while" the_function in
      ignore (L.build_br pred_bb builder);

      let body_bb = L.append_block context "list_iter_body" the_function in
      let body_builder = L.builder_at_end context body_bb in
        (* Set the node pointer to the next node and the index pointer to i-1 *)
        let node = L.build_load node_ptr "node" body_builder in
        let next_node = extract_node_node node body_builder in
        let i = L.build_load i_ptr "i" body_builder in
        let tmp_i = L.build_sub i one "tmp_i" body_builder in
        ignore (L.build_store next_node node_ptr body_builder);
        ignore (L.build_store tmp_i i_ptr body_builder);
        ignore (L.build_br pred_bb body_builder);

      let pred_builder = L.builder_at_end context pred_bb in
      let bool_val =
        (* End iteration if i is 0 *)
        let i = L.build_load i_ptr "i" pred_builder in
        L.build_icmp L.Icmp.Ne i zero "not_zero" pred_builder in

      let merge_bb = L.append_block context "merge" the_function in
      ignore (L.build_cond_br bool_val body_bb merge_bb pred_builder);
      L.position_at_end merge_bb builder;

      L.build_load node_ptr "node" builder;
    in

    let list_iter builder node_ptr i_ptr =
      let node = list_iter_node builder node_ptr i_ptr in
      extract_node_struct node builder
    in

    (* Function for list access *)
    (* Takes a list node and a float *)
    let access_list builder node num =
      (* Helper functions to ensure that the index is a nonnegative integer *)
      let is_int = L.build_fcmp L.Fcmp.Oeq
        (L.build_frem num (L.const_float double_t 1.0) "mod_one" builder)
        (L.const_float double_t 0.0)
        "is_int" builder in
      let is_nonneg = L.build_fcmp L.Fcmp.Oge
        num
        (L.const_float double_t 0.0)
        "is_nonneg" builder in
      let int_num = L.build_fptoui (L.build_fadd num
        (L.const_float double_t 0.5)
        "num_mod" builder) i32_t "int_num" builder in
      let int_num_ptr = L.build_malloc i32_t "int_num_ptr" builder in
      ignore (L.build_store int_num int_num_ptr builder);

      let bool_val = L.build_and is_int is_nonneg "bool_val" builder in
      let merge_bb = L.append_block context "merge_index_check" the_function in
      let good_index_bb = L.append_block context "good_index" the_function in
      let bad_index_bb = L.append_block context "bad_index" the_function in

      (* Pointer to the data struct that will be returned (or set to null if the
         index is bad) *)
      let data_ptr = L.build_malloc (pointer_t struct_t) "data_ptr"
        builder in

      let node_ptr = L.build_malloc (pointer_t node_t) "node_ptr"
        builder in
      ignore (L.build_store node node_ptr builder);

      let good_index_builder = L.builder_at_end context good_index_bb in
        ignore (L.build_store (list_iter good_index_builder node_ptr
          int_num_ptr) data_ptr good_index_builder);
        ignore (L.build_br merge_bb good_index_builder);

      let bad_index_builder = L.builder_at_end context bad_index_bb in
        ignore (L.build_call printf_func [| string_format_str; bad_index_str |]
          "printf" bad_index_builder);
        ignore (L.build_call abort_func [||] "" bad_index_builder);
        ignore (L.build_br merge_bb bad_index_builder);

    ignore (L.build_cond_br bool_val good_index_bb bad_index_bb builder);
      L.position_at_end merge_bb builder;
      L.build_load data_ptr "data" builder;
    in

    let map_iter builder node_ptr str_ptr =
      let pred_bb = L.append_block context "map_while" the_function in
      let pred_continue_bb = L.append_block context "map_while_continue"
        the_function in
      let map_end_bb = L.append_block context "map_end" the_function in
      let merge_bb = L.append_block context "merge" the_function in

      ignore (L.build_br pred_bb builder);

      let body_bb = L.append_block context "map_iter_body" the_function in
      let body_builder = L.builder_at_end context body_bb in
        (* Set the node pointer to the next node and the index pointer to i-1 *)
        let node = L.build_load node_ptr "node" body_builder in
        let next_node = extract_node_node node body_builder in
        ignore (L.build_store next_node node_ptr body_builder);
        ignore (L.build_br pred_bb body_builder);

      let pred_builder = L.builder_at_end context pred_bb in

      let node = L.build_load node_ptr "node_ptr" pred_builder in

      let map_end_builder = L.builder_at_end context map_end_bb in
        (* Add a node to the map and return an empty struct *)
        let node_string_ptr = L.build_in_bounds_gep node [| zero; zero |]
          "node_string_ptr" map_end_builder in
        let node_struct_ptr = L.build_in_bounds_gep node [| zero; one |]
          "node_struct_ptr" map_end_builder in
        let node_node_ptr = L.build_in_bounds_gep node [| zero; two |]
          "node_node_ptr" map_end_builder in
        let new_struct = build_empty_struct map_end_builder "new_struct" in
        let new_node = build_empty_node map_end_builder "new_node" in
        ignore (L.build_store str_ptr node_string_ptr map_end_builder);
        ignore (L.build_store new_struct node_struct_ptr map_end_builder);
        ignore (L.build_store new_node node_node_ptr map_end_builder);
      ignore (L.build_br merge_bb map_end_builder);

      let node_struct = extract_node_struct node pred_builder in
      let map_end = L.build_is_null node_struct "map_end" pred_builder in
      ignore (L.build_cond_br map_end map_end_bb pred_continue_bb pred_builder);
      L.position_at_end pred_continue_bb pred_builder;

      let node_str = extract_node_string node pred_builder in
      let node_str_begin = L.build_in_bounds_gep node_str [| zero; zero |]
        "node_str_begin" pred_builder in (* Pointer to first char in str *)
      (* Pointer to the fist char of comparison str *)
      let str_begin = L.build_in_bounds_gep str_ptr [| zero; zero |]
        "str_begin" pred_builder in
      let strcmp_val = L.build_call strcmp_func [| node_str_begin; str_begin |]
        "strcmp" pred_builder in
      let bool_val = L.build_icmp L.Icmp.Ne strcmp_val zero "str_eq"
        pred_builder in

      ignore (L.build_cond_br bool_val body_bb merge_bb pred_builder);
      L.position_at_end merge_bb builder;

      let node = L.build_load node_ptr "node" builder in
      extract_node_struct node builder
    in

    (* Function for map access *)
    let access_map builder node str_ptr =
      let node_ptr = (L.build_malloc (pointer_t node_t) "node_ptr"
        builder) in
      ignore (L.build_store node node_ptr builder);

      map_iter builder node_ptr str_ptr
    in

    let access_list_or_map builder lm i =
      let struct_index = valid_struct_index i builder in

      let bool_val = L.build_icmp L.Icmp.Eq struct_index zero "is_num"
        builder in
      let merge_bb = L.append_block context "merge_access" the_function in
      let access_list_bb = L.append_block context "access_list" the_function in
      let access_map_bb = L.append_block context "access_map" the_function in

      let data_ptr = L.build_malloc (pointer_t struct_t) "data_ptr"
        builder in

      let access_list_builder = L.builder_at_end context access_list_bb in
        let lst = extract_list lm access_list_builder in
        let num = extract_num_val i access_list_builder in
        ignore (L.build_store (access_list access_list_builder lst num) data_ptr
          access_list_builder);
        ignore (L.build_br merge_bb access_list_builder);

      let access_map_builder = L.builder_at_end context access_map_bb in
        let map = extract_map lm access_map_builder in
        let key_ptr = extract_string i access_map_builder in
        ignore (L.build_store (access_map access_map_builder map key_ptr)
          data_ptr access_map_builder);
        ignore (L.build_br merge_bb access_map_builder);

      ignore (L.build_cond_br bool_val access_list_bb access_map_bb builder);
      L.position_at_end merge_bb builder;
      L.build_load data_ptr "data" builder
    in

    let remove_map str_struct node_struct builder =
      let node_ptr = L.build_malloc (pointer_t node_t) "node_ptr" builder in
      let map = extract_map node_struct builder in
      let str_ptr = extract_string str_struct builder in
      ignore (L.build_store map node_ptr builder);

      let prev_node_next_ptr_ptr = L.build_malloc (pointer_t node_t)
        "prev_node_next_ptr_ptr" builder in
      ignore (L.build_store map prev_node_next_ptr_ptr builder);

      let pred_bb = L.append_block context "map_while" the_function in
      let pred_continue_bb = L.append_block context "map_while_continue"
        the_function in
      let merge_bb = L.append_block context "merge" the_function in

      ignore (L.build_br pred_bb builder);

      let body_bb = L.append_block context "map_iter_body" the_function in
      let body_builder = L.builder_at_end context body_bb in
        (* Set the node pointer to the next node and the index pointer to i-1 *)
        let node = L.build_load node_ptr "node" body_builder in
        let next_node = extract_node_node node body_builder in
        ignore (L.build_store next_node prev_node_next_ptr_ptr body_builder);
        ignore (L.build_store next_node node_ptr body_builder);
        ignore (L.build_br pred_bb body_builder);

      let pred_builder = L.builder_at_end context pred_bb in

      let node = L.build_load node_ptr "node_ptr" pred_builder in
      let node_struct = extract_node_struct node pred_builder in
      let map_end = L.build_is_null node_struct "map_end" pred_builder in
      ignore (L.build_cond_br map_end merge_bb pred_continue_bb pred_builder);
      L.position_at_end pred_continue_bb pred_builder;

      let node_str = extract_node_string node pred_builder in
      let node_str_begin = L.build_in_bounds_gep node_str [| zero; zero |]
        "node_str_begin" pred_builder in (* Pointer to first char in str *)
      (* Pointer to the fist char of comparison str *)
      let str_begin = L.build_in_bounds_gep str_ptr [| zero; zero |]
        "str_begin" pred_builder in
      let strcmp_val = L.build_call strcmp_func [| node_str_begin; str_begin |]
        "strcmp" pred_builder in
      let bool_val = L.build_icmp L.Icmp.Ne strcmp_val zero "str_eq"
        pred_builder in
      ignore (L.build_cond_br bool_val body_bb merge_bb pred_builder);

      L.position_at_end merge_bb builder;

      let node = L.build_load node_ptr "node" builder in
      let next_node_ptr = extract_node_node node builder in
      let next_node = L.build_load next_node_ptr "next_node" builder in
      let prev_node_next_ptr = L.build_load prev_node_next_ptr_ptr
        "prev_node_next_ptr" builder in
      ignore (L.build_store next_node prev_node_next_ptr builder);
    in

    (* Takes two structs, type checks to see if they are number and list
       respectively then removes the item represented by the number in i *)
    let remove_list i lst builder =
      let node = extract_list lst builder in
      let node_ptr = (L.build_malloc (pointer_t node_t) "node_ptr"
        builder) in
      let node_ptr2 = (L.build_malloc (pointer_t node_t) "node_ptr"
        builder) in
      ignore (L.build_store node node_ptr builder);
      ignore (L.build_store node node_ptr2 builder);

      (* Get node to be removed TODO error check out of bounds  *)
      let i_val = extract_num_val i builder in
      let i_int = L.build_fptoui i_val i32_t "int_num" builder in

      let i_ptr = L.build_malloc i32_t "ret_ptr" builder in
      ignore( L.build_store i_int i_ptr builder );

      let prev_node_next_ptr = L.build_malloc (pointer_t node_t) "prev_ptr"
        builder in
      let node_i = list_iter_node builder node_ptr i_ptr in

      (* Determine if we are extracting zero'th element *)
      let pred_expr = extract_num_val i builder in
      let bool_val = L.build_fcmp L.Fcmp.Ole pred_expr f_zero "is_remove_zero"
        builder in
      let merge_bb = L.append_block context "remove_merge" the_function in

      let j_ptr = L.build_malloc i32_t "ret_ptr" builder in
      let then_bb = L.append_block context "then_remove_zero" the_function in
      let then_builder = L.builder_at_end context then_bb in
      (* get pointer to first element *)
        ignore( L.build_store (extract_list lst then_builder) prev_node_next_ptr
          then_builder );
        ignore( L.build_br merge_bb then_builder);

      let else_bb = L.append_block context "else" the_function in
      let else_builder = L.builder_at_end context else_bb in
        (* One to the left of one to be removed  *)
        let j = L.build_sub i_int one "sub" else_builder in
        ignore( L.build_store j j_ptr else_builder );
        let node_j = list_iter_node else_builder node_ptr2 j_ptr in
        let next_node_pointer_j = extract_node_node node_j else_builder in
        ignore( L.build_store next_node_pointer_j prev_node_next_ptr
          else_builder );
        ignore (L.build_br merge_bb else_builder);

      ignore (L.build_cond_br bool_val then_bb else_bb builder);
      ignore( L.position_at_end merge_bb builder);

      let next_node_pointer_i = extract_node_node node_i builder in
      let next_node_i = L.build_load next_node_pointer_i "next_node_i"
        builder in
      let prev_node_next = L.build_load prev_node_next_ptr "prev_node_next"
        builder in
      ignore( L.build_store next_node_i prev_node_next builder );
    in

    (* handle_plus operator returns the appropriate struct when applying '+' to
       two structs s1 and s2. handle_plus also takes the type of the structs as
       a i32 in type_structs to determine which functionality to apply
*)
    let handle_plus struct_type s1 s2 builder =
      let ret_ptr = L.build_malloc (pointer_t struct_t) "ret_ptr" builder in
      let merge_bb = L.append_block context "merge_equal" the_function in

      let plus_num_bb = L.append_block context "plus_num" the_function in
      let plus_str_bb = L.append_block context "plus_str" the_function in

      let num_builder = L.builder_at_end context plus_num_bb in
        let val1 = extract_num_val s1 num_builder and
            val2 = extract_num_val s2 num_builder in
         ignore( L.build_store ( construct_num (L.build_fadd val1 val2 "add"
           num_builder) num_builder ) ret_ptr num_builder);
         ignore (L.build_br merge_bb num_builder);

      let str_builder = L.builder_at_end context plus_str_bb in
        let str1 = extract_string s1 str_builder and
            str2 = extract_string s2 str_builder in
        let char_ptr1 = L.build_in_bounds_gep str1 [| zero; zero |]
              "char_ptr" str_builder and
            char_ptr2 = L.build_in_bounds_gep str2 [| zero; zero |]
              "char_ptr" str_builder in
        let len1 = L.build_call strlen_func [| char_ptr1 |] "len1"
              str_builder and
            len2 = L.build_call strlen_func [| char_ptr2 |] "len2"
              str_builder in
        (* Allocate memory for new string of just the right size *)
        let size = L.build_add (L.build_add len1 len2 "sum" str_builder) one
          "sum" str_builder in
        let ret_str = L.build_malloc string_t "str_cat_alloc" builder in
        let ret_str_start = L.build_in_bounds_gep ret_str [| zero; zero |]
          "ret_str_start" str_builder in
          (* Move the first string to the return location *)
          ignore( L.build_call strcpy_func [| ret_str_start; char_ptr1 |]
            "strcpy" str_builder );
          (* Do the string concatenation *)
          ignore( L.build_call strcat_func [| ret_str_start; char_ptr2 |]
            "strcat" str_builder );
          (* Load the return string into the return pointer *)
          ignore( L.build_store ( construct_str ret_str str_builder ) ret_ptr
            str_builder );
          ignore (L.build_br merge_bb str_builder);

      let switch = L.build_switch struct_type merge_bb 2 builder in
         L.add_case switch zero plus_num_bb; (* Plus on two numbers *)
        L.add_case switch one  plus_str_bb; (* Plus on two strings *)

      L.position_at_end merge_bb builder;
      L.build_load ret_ptr "data" builder
    in

    (* Assignment helper. d is an expression; s is a struct. *)
    let rec assign d s builder =
        (match d with
          A.Id x ->
            (if (StringHash.mem local_vars x) then
              let new_val = L.build_load s "new_val" builder in
              let var = StringHash.find local_vars x in
              ignore (L.build_store new_val var builder)
            else
              let new_val = L.build_load s "new_val" builder in
              let var = L.build_malloc struct_t "var" malloc_builder in
              ignore (L.build_store new_val var builder);
              ignore (StringHash.add local_vars x var)); s
        | _ ->
            let new_val = L.build_load s "new_val" builder in
            let d' = expr builder d in
            ignore (L.build_store new_val d' builder); s
        )
    (* Handle YAGL expressions *)
    and expr builder = function
      (* Construct an empty structure for NULL *)
       A.Null -> build_empty_struct builder "NULL"
      (* Given a number, create a struct containing that number *)
     | A.Num i ->
        let num_ptr = L.define_global (string_of_float i)
                                      (L.const_float double_t i)
                                      the_module in
        let num_struct = build_empty_struct builder "num_struct" in
        let struct_num = L.build_in_bounds_gep num_struct [| zero; zero |]
          "struct_num" builder in
        ignore (L.build_store num_ptr struct_num builder); num_struct
      (* Given a string, create a struct containing that string *)
    | A.String s ->
        let str_ptr =
          (L.const_bitcast (L.build_global_stringptr s s builder)
                           (pointer_t string_t)) in
        let str_struct = build_empty_struct builder "str_struct" in
        let struct_str = L.build_in_bounds_gep str_struct [| zero; one |]
          "struct_str" builder in
        ignore (L.build_store str_ptr struct_str builder); str_struct
      (* Given a list, create a struct containing that list *)
    | A.List l ->
        let node = build_empty_node builder "node" in
        let list_struct = build_empty_struct builder "list_struct" in
        let struct_list = L.build_in_bounds_gep list_struct [| zero; two |]
          "struct_list" builder in
        ignore (L.build_store node struct_list builder);
        (* Recursively construct the list *)
        let rec make_list l node = (match l with
          [] -> ();
        | head :: tail ->
            let node_struct = L.build_in_bounds_gep node [| zero; one |]
              "node_struct" builder in
            let node_node = L.build_in_bounds_gep node [| zero; two |]
              "node_node" builder in
            let next_node = (build_empty_node builder "next_node") in
            ignore (L.build_store (expr builder head) node_struct builder);
            ignore (L.build_store next_node node_node builder);
            make_list tail next_node
        )
        in make_list l node;
        list_struct
    | A.Map m ->
        let node = build_empty_node builder "node" in
        let map_struct = build_empty_struct builder "map_struct" in
        let struct_map = L.build_in_bounds_gep map_struct [| zero; three |]
          "struct_map" builder in
        ignore (L.build_store node struct_map builder);
        (* Recursively construct the map *)
        let rec make_map m node = (match m with
          [] -> ();
        | head :: tail ->
            let key = fst head in
            let value = snd head in
            let node_string = L.build_in_bounds_gep node [| zero; zero |]
              "node_string" builder in
            let node_struct = L.build_in_bounds_gep node [| zero; one |]
              "node_struct" builder in
            let node_node = L.build_in_bounds_gep node [| zero; two |]
              "node_node" builder in
            let next_node = (build_empty_node builder "next_node") in
            let str_ptr =
              (L.const_bitcast (L.build_global_stringptr key key builder)
                               (pointer_t string_t)) in
            ignore (L.build_store str_ptr node_string builder);
            ignore (L.build_store (expr builder value) node_struct builder);
            ignore (L.build_store next_node node_node builder);
            make_map tail next_node
        )
        in make_map m node;
        map_struct
      (* TODO: kill graph; rename digraph to graph *)
    | A.Graph -> construct_graph builder
    | A.Digraph -> construct_graph builder
      (* Retrieve a variable, loading it from its pointer under the hood. *)
    | A.Id s -> (try StringHash.find local_vars s
                 with Not_found -> raise (Uninitialized_var(s)))
    | A.Unop ( op, e1 ) ->  (* Handle unary operators *)

        let s1 = expr builder e1 in
        ignore( check_struct_type s1 num_type builder );
        let e1' = extract_num_val s1 builder in
        ( match op with
          A.Neg -> construct_num (L.build_fneg e1' "neg" builder) builder
        | A.Not -> construct_num (L.build_uitofp (L.build_not (
            L.build_fcmp L.Fcmp.One e1' f_zero
            "neq" builder) "not" builder ) double_t "fp" builder ) builder
        )
    | A.Eqop( e1, op, e2 ) -> (* Handle comparison operators *)
        let s1 = expr builder e1 and
            s2 = expr builder e2 in
        ( match op with
        A.Equal -> is_equal s1 s2 builder
        | A.Neq -> construct_num ( L.build_uitofp (
            L.build_not ( L.build_fcmp L.Fcmp.One (extract_num_val
              (is_equal s1 s2 builder) builder ) f_zero
            "neq" builder) "not" builder ) double_t "fp" builder ) builder
        )
    | A.Binop (e1, op, e2) ->  (* Handle binary operators *)
        (* Stuct_types is an int indicating the types of the two expressions
           for use in overridden operator switch *)
        let s1 = expr builder e1 and
            s2 = expr builder e2 in
        (* Ensures the two expressions are of the same type *)
        let struct_type = type_comparison s1 s2 builder in
        (* Move num type checking down to lower level matches later as necessary *)
        if op == A.Add then handle_plus struct_type s1 s2 builder
        else ( ignore( check_index_type struct_type num_type builder );
        let e1' = extract_num_val (expr builder e1) builder and
            e2' = extract_num_val (expr builder e2) builder in
        (match op with
          A.Sub -> construct_num (L.build_fsub e1' e2' "sub" builder) builder
        | A.Mult -> construct_num (L.build_fmul e1' e2' "mult" builder) builder
        | A.Div -> construct_num (L.build_fdiv e1' e2' "div" builder) builder
        | A.Mod -> let unadjusted_mod = L.build_frem e1' e2' "mod" builder in
                   let res_non_zero   = L.build_fcmp L.Fcmp.One unadjusted_mod
                     f_zero "res_non_zero" builder in
                   let lval_is_neg    = L.build_fcmp L.Fcmp.Olt e1' f_zero
                     "negative" builder  in
                   let adj_bool      = L.build_and lval_is_neg res_non_zero
                     "and" builder in
                   let f_bool        = L.build_uitofp adj_bool double_t "fp"
                     builder in
                   (* Only adjust by rval if result is non-zero and lval < 0 *)
                   let adjust_factor = L.build_fmul f_bool e2' "scale"
                     builder in
                   let ret = L.build_fadd unadjusted_mod adjust_factor "adjust"
                     builder in
                   construct_num ret builder
        | A.Less -> construct_num (L.build_uitofp (L.build_fcmp L.Fcmp.Olt
            e1' e2' "less" builder) double_t "fp" builder) builder
        | A.Leq -> construct_num (L.build_uitofp (L.build_fcmp L.Fcmp.Ole
            e1' e2' "leq" builder) double_t "fp" builder) builder
        | A.Greater -> construct_num (L.build_uitofp (L.build_fcmp L.Fcmp.Ogt
            e1' e2' "greater" builder) double_t "fp" builder) builder
        | A.Geq -> construct_num (L.build_uitofp (L.build_fcmp L.Fcmp.Oge
            e1' e2' "geq" builder) double_t "fp" builder) builder
        | A.And -> construct_num (L.build_uitofp (L.build_and
            (L.build_fcmp L.Fcmp.One e1' (L.const_float double_t 0.0) "neq"
              builder)  (* Map first and second *)
            (L.build_fcmp L.Fcmp.One e2' (L.const_float double_t 0.0) "neq"
              builder)  (* Expression to canonical *)
            (* Truth values then and *)
            "and" builder) double_t "fp" builder) builder
        | A.Or -> construct_num (L.build_uitofp (L.build_or
            (L.build_fcmp L.Fcmp.One e1' (L.const_float double_t 0.0) "neq"
              builder)  (* Map first and second *)
            (L.build_fcmp L.Fcmp.One e2' (L.const_float double_t 0.0) "neq"
              builder)  (* Expression to canonical *)
            (* Truth values then or *)
            "or" builder) double_t "fp" builder) builder
        ) )
    (* Assign a variable. If it is new, mallocte a pointer pointing to it. If it
       exists and is being reassigned, redirect the pointer. *)
    | A.Assign (d, e) -> assign d (expr builder e) builder
      (* Access a list by an index *)
    | A.ListOrMapAccess (d, e) ->
        access_list_or_map builder (expr builder d) (expr builder e)
      (* Access a map by string *)
    | A.MapStringAccess (d, s) ->
        let str_ptr =
          (L.const_bitcast (L.build_global_stringptr s s builder)
                           (pointer_t string_t)) in
        let map_struct = expr builder d in
        ignore( check_struct_type map_struct map_type builder );
        let map = extract_map map_struct builder in
          access_map builder map str_ptr
      (* Call print *)
      (* Note print returns the struct it printed *)
    | A.Call ("print", [e]) -> let e' = expr builder e in
        print_struct builder e'; e'
    | A.Call ("addV", [d; e]) ->
        let g_struct = expr builder d and
            s_struct = expr builder e in
        ignore( check_struct_type g_struct graph_type builder );
        ignore( check_struct_type s_struct str_type builder );
        let g = extract_graph g_struct builder and
            s = extract_string s_struct builder in
        let vs = extract_graph_v g builder in
        let v = access_map builder vs s in
        (* TODO check if v's struct is already initialized *)
        let v_attr_map_ptr = L.build_in_bounds_gep v [| zero; three |]
          "v_attr_map_ptr" builder in
        let v_attr_map = L.build_load v_attr_map_ptr "v_attr_map" builder in

        let merge_bb = L.append_block context "merge" the_function in

        let map_ptr = L.build_malloc (pointer_t node_t) "map_ptr" builder in

        let then_bb = L.append_block context "then" the_function in
        let then_builder = L.builder_at_end context then_bb in
        let new_map = build_empty_node then_builder "map" in
        ignore (L.build_store new_map map_ptr then_builder);
        ignore (L.build_store new_map v_attr_map_ptr then_builder);
        ignore (L.build_br merge_bb then_builder);

        let else_bb = L.append_block context "else" the_function in
        let else_builder = L.builder_at_end context else_bb in
        let curr_map = extract_map v else_builder in
        ignore (L.build_store curr_map map_ptr else_builder);
        ignore (L.build_br merge_bb else_builder);

        let is_null = L.build_is_null v_attr_map "is_null" builder in
        ignore (L.build_cond_br is_null then_bb else_bb builder);
        L.position_at_end merge_bb builder;

        let map = L.build_load map_ptr "map" builder in

        let label_str = extract_string (expr builder (A.String "label"))
          builder in
        let label_struct = access_map builder map label_str in
        let str_ptr = L.build_in_bounds_gep label_struct [| zero; one |]
          "str_ptr" builder in
        ignore (L.build_store s str_ptr builder);
        v
    | A.Call ("v", [e]) ->
        let g_struct = expr builder e in
        let g = extract_graph g_struct builder in
        let vs = extract_graph_v g builder in
        ignore( check_struct_type g_struct graph_type builder );
        let ret_struct = build_empty_struct builder "ret_struct" in
        (* TODO this return structure doesn't seem right *)
        let map_ptr = L.build_in_bounds_gep ret_struct [| zero; three |]
          "map_ptr" builder in
        ignore (L.build_store vs map_ptr builder);
        ret_struct
    | A.Call ("addE", [c; d; e]) ->
        let g_struct = expr builder c and
            s_struct = handle_plus one
              (handle_plus one (expr builder d)
                               (expr builder (A.String "~")) builder)
              (expr builder e) builder and
            s1_struct = expr builder d and
            s2_struct = expr builder e in
        ignore( check_struct_type g_struct graph_type builder );
        ignore( check_struct_type s_struct str_type builder );
        ignore (expr builder (A.Call ("addV", [c; d])));
        ignore (expr builder (A.Call ("addV", [c; e])));
        let g = extract_graph g_struct builder and
            s = extract_string s_struct builder and
            s1 = extract_string s1_struct builder and
            s2 = extract_string s2_struct builder in
        let es = extract_graph_e g builder in
        let e = access_map builder es s in

        let e_attr_map_ptr = L.build_in_bounds_gep e [| zero; three |]
          "e_attr_map_ptr" builder in
        let e_attr_map = L.build_load e_attr_map_ptr "e_attr_map" builder in

        let map_ptr = L.build_malloc (pointer_t node_t) "map_ptr" builder in

        let merge_bb = L.append_block context "merge" the_function in

        let then_bb = L.append_block context "then" the_function in
        let then_builder = L.builder_at_end context then_bb in
        let new_map = build_empty_node builder "new_map" in
        ignore (L.build_store new_map e_attr_map_ptr then_builder);
        ignore (L.build_store new_map map_ptr then_builder);
        ignore (L.build_br merge_bb then_builder);

        let else_bb = L.append_block context "else" the_function in
        let else_builder = L.builder_at_end context else_bb in
        let curr_map = extract_map e else_builder in
        ignore (L.build_store curr_map map_ptr else_builder);
        ignore (L.build_br merge_bb else_builder);

        let is_null = L.build_is_null e_attr_map "is_null" builder in
        ignore (L.build_cond_br is_null then_bb else_bb builder);
        L.position_at_end merge_bb builder;

        let map = L.build_load map_ptr "map" builder in

        let vs = extract_graph_v g builder in
        let v0_map = extract_map (access_map builder vs s1) builder in
        let v0_str = extract_string (expr builder (A.String "orig")) builder in
        let e_v0_struct = access_map builder map v0_str in
        let e_v0_ptr = L.build_in_bounds_gep e_v0_struct [| zero; three |]
          "e_v0_ptr" builder in
        ignore (L.build_store v0_map e_v0_ptr builder);
        let v1_map = extract_map (access_map builder vs s2) builder in
        let v1_str = extract_string (expr builder (A.String "dest")) builder in
        let e_v1_struct = access_map builder map v1_str in
        let e_v1_ptr = L.build_in_bounds_gep e_v1_struct [| zero; three |]
          "e_v1_ptr" builder in
        ignore (L.build_store v1_map e_v1_ptr builder);
        e
    | A.Call ("e", [e]) ->
        let g_struct = expr builder e in
        ignore( check_struct_type g_struct graph_type builder );
        (* TODO also take graphs (not just digraphs) into account *)
        let g = extract_graph g_struct builder in
        let es = extract_graph_e g builder in
        let ret_struct = build_empty_struct builder "ret_struct" in
        (* TODO this return structure doesn't seem right *)
        let map_ptr = L.build_in_bounds_gep ret_struct [| zero; three |]
          "map_ptr" builder in
        ignore (L.build_store es map_ptr builder);
        ret_struct
      (* Call any other function *)
    | A.Call ("append", [d; e]) ->
        let l_struct = expr builder e in
        let e_struct = expr builder d in
        ignore( check_struct_type l_struct lst_type builder );
        list_append l_struct e_struct builder; l_struct
      (* Remove expects an index and a list struct *)
    | A.Call ("remove", [i; ls]) ->
        let ls' = expr builder ls and
            i' = expr builder i  in
        let merge_bb = L.append_block context "merge_remove" the_function in

        let ret_ptr = L.build_malloc (pointer_t struct_t) "ret_ptr" builder in

        let bad_type_bb = L.append_block context "bad_type" the_function in
        let bad_type_builder = L.builder_at_end context bad_type_bb in
        ignore (L.build_call printf_func [| right_type_str |] "printf"
          bad_type_builder);
        ignore (L.build_call abort_func [||] "" bad_type_builder);
        ignore (L.build_br merge_bb bad_type_builder);

        let list_bb = L.append_block context "remove_list" the_function in
        let map_bb = L.append_block context "remove_map" the_function in

        let list_builder = L.builder_at_end context list_bb in
        let ret_val = access_list_or_map list_builder ls' i' in
        ignore( check_struct_type ls' lst_type list_builder );
        ignore( remove_list i' ls' list_builder );
        (*  let update_node = remove_list i' ls' in *)
        (* now you set the nxt_node pointer on update node to be the nxt_node
           pointer on ret_val *)
        ignore (L.build_store ret_val ret_ptr list_builder);
        ignore (L.build_br merge_bb list_builder);

        let map_builder = L.builder_at_end context map_bb in
        let ret_val = access_list_or_map map_builder ls' i' in
        ignore( check_struct_type ls' map_type map_builder );
        ignore (remove_map i' ls' map_builder);
        ignore (L.build_store ret_val ret_ptr map_builder);
        ignore (L.build_br merge_bb map_builder);

        let i_type = valid_struct_index i' builder in
        let switch = L.build_switch i_type bad_type_bb 2 builder in
          L.add_case switch zero list_bb;
          L.add_case switch one map_bb;

        L.position_at_end merge_bb builder;
        L.build_load ret_ptr "ret_val" builder;
    | A.Call (f, act) -> let (fdef, _) = StringMap.find f function_decls in
        let actuals = List.map (expr builder) act in
        let result = f ^ "_result" in
        L.build_call fdef (Array.of_list actuals) result builder
    | A.Typeof(e)->let e' = expr builder e in
                   let struct_type = valid_struct_index e' builder in
                   construct_num (L.build_sitofp struct_type double_t "fp"
                     builder)  builder
    | A.Noexpr -> build_empty_struct builder "empty_struct"
    in

    let rec build_for_kv_list k v e body builder =
      let s = expr builder e in
      let node_ptr = L.build_malloc (pointer_t node_t) "node_ptr"
        builder in
      let i_ptr = L.build_malloc double_t "i_ptr" builder in
      ignore (L.build_store (extract_list s builder) node_ptr builder);
      ignore (L.build_store f_zero i_ptr builder);

      let pred_bb = L.append_block context "for_each_while" the_function in
      ignore (L.build_br pred_bb builder);

      let body_bb = L.append_block context "for_each_iter_body" the_function in
      let body_builder = L.builder_at_end context body_bb in
        let i = L.build_load i_ptr "i" body_builder in
        let next_i = L.build_fadd i f_one "next_i" body_builder in
        let node = L.build_load node_ptr "node" body_builder in
        let next_node = extract_node_node node body_builder in
        let node_struct = extract_node_struct node body_builder in
        ignore (assign k (construct_num i body_builder) body_builder);
        ignore (assign v node_struct body_builder);
        ignore (L.build_store next_node node_ptr body_builder);
        ignore (L.build_store next_i i_ptr body_builder);
      let body_builder = (List.fold_left stmt body_builder body) in
      add_terminal body_builder (L.build_br pred_bb);

      let pred_builder = L.builder_at_end context pred_bb in

      let node = L.build_load node_ptr "node_ptr" pred_builder in
      let node_struct = extract_node_struct node pred_builder in
      let bool_val = L.build_is_not_null node_struct "bool_val" pred_builder in

      let merge_bb = L.append_block context "merge_for_each" the_function in
      ignore (L.build_cond_br bool_val body_bb merge_bb pred_builder);
      L.position_at_end merge_bb builder;

    and build_for_kv_map k v e body builder =
      let s = expr builder e in
      let node_ptr = L.build_malloc (pointer_t node_t) "node_ptr"
        builder in
      ignore (L.build_store (extract_map s builder) node_ptr builder);

      let pred_bb = L.append_block context "for_each_while" the_function in
      ignore (L.build_br pred_bb builder);

      let body_bb = L.append_block context "for_each_iter_body" the_function in
      let body_builder = L.builder_at_end context body_bb in
        let node = L.build_load node_ptr "node" body_builder in
        let next_node = extract_node_node node body_builder in
        let node_str = extract_node_string node body_builder in
        let node_struct = extract_node_struct node body_builder in
        let str_struct = construct_str node_str body_builder in
        ignore (assign k str_struct body_builder);
        ignore (assign v node_struct body_builder);
        ignore (L.build_store next_node node_ptr body_builder);
        let body_builder = (List.fold_left stmt body_builder body) in
      add_terminal body_builder (L.build_br pred_bb);

      let pred_builder = L.builder_at_end context pred_bb in

      let node = L.build_load node_ptr "node_ptr" pred_builder in
      let node_struct = extract_node_struct node pred_builder in
      let bool_val = L.build_is_not_null node_struct "bool_val" pred_builder in

      let merge_bb = L.append_block context "merge_for_each" the_function in
      ignore (L.build_cond_br bool_val body_bb merge_bb pred_builder);
      L.position_at_end merge_bb builder;

    and build_for_each_list x e body builder =
      let s = expr builder e in
      let node_ptr = L.build_malloc (pointer_t node_t) "node_ptr"
        builder in
      let i_ptr = L.build_malloc double_t "i_ptr" builder in
      ignore (L.build_store (extract_list s builder) node_ptr builder);
      ignore (L.build_store f_zero i_ptr builder);

      let pred_bb = L.append_block context "for_each_while" the_function in
      ignore (L.build_br pred_bb builder);

      let body_bb = L.append_block context "for_each_iter_body" the_function in
      let body_builder = L.builder_at_end context body_bb in
        let i = L.build_load i_ptr "i" body_builder in
        let next_i = L.build_fadd i f_one "next_i" body_builder in
        let node = L.build_load node_ptr "node" body_builder in
        let next_node = extract_node_node node body_builder in
        ignore (assign x (construct_num i body_builder) body_builder);
        ignore (L.build_store next_node node_ptr body_builder);
        ignore (L.build_store next_i i_ptr body_builder);
      let body_builder = (List.fold_left stmt body_builder body) in
      add_terminal body_builder (L.build_br pred_bb);

      let pred_builder = L.builder_at_end context pred_bb in

      let node = L.build_load node_ptr "node_ptr" pred_builder in
      let node_struct = extract_node_struct node pred_builder in
      let bool_val = L.build_is_not_null node_struct "bool_val" pred_builder in

      let merge_bb = L.append_block context "merge_for_each" the_function in
      ignore (L.build_cond_br bool_val body_bb merge_bb pred_builder);
      L.position_at_end merge_bb builder;

    and build_for_each_map x e body builder =
      let s = expr builder e in
      let node_ptr = L.build_malloc (pointer_t node_t) "node_ptr"
        builder in
      ignore (L.build_store (extract_map s builder) node_ptr builder);

      let pred_bb = L.append_block context "for_each_while" the_function in
      ignore (L.build_br pred_bb builder);

      let body_bb = L.append_block context "for_each_iter_body" the_function in
      let body_builder = L.builder_at_end context body_bb in
        let node = L.build_load node_ptr "node" body_builder in
        let next_node = extract_node_node node body_builder in
        let node_str = extract_node_string node body_builder in
        let str_struct = construct_str node_str body_builder in
        ignore (assign x str_struct body_builder);
        ignore (L.build_store next_node node_ptr body_builder);
        let body_builder = (List.fold_left stmt body_builder body) in
      add_terminal body_builder (L.build_br pred_bb);

      let pred_builder = L.builder_at_end context pred_bb in

      let node = L.build_load node_ptr "node_ptr" pred_builder in
      let node_struct = extract_node_struct node pred_builder in
      let bool_val = L.build_is_not_null node_struct "bool_val" pred_builder in

      let merge_bb = L.append_block context "merge_for_each" the_function in
      ignore (L.build_cond_br bool_val body_bb merge_bb pred_builder);
      L.position_at_end merge_bb builder;

    (* Handle YAGL statements *)
    and stmt builder = function
      (* Handle a simple expression *)
      A.Expr e -> ignore (expr builder e); builder
      (* Return void if this is the main function; otherwise, return the value
         represented by the expression *)
      (* TODO: add a semantic check that main doesn't return *)
    | A.Return e -> ignore (match fdecl.A.fname with
          "main" -> L.build_ret_void builder
        | _ -> L.build_ret (expr builder e) builder); builder
      (* If/else handling and block construction *)
    | A.If (predicate, then_stmts, else_stmts) ->
        let pred_struct = expr builder predicate in
        ignore( check_struct_type pred_struct num_type builder );
        let pred_expr = extract_num_val pred_struct builder in
        let bool_val = is_true pred_struct builder in
        let merge_bb = L.append_block context "merge" the_function in

        let then_bb = L.append_block context "then" the_function in
        add_terminal (
          List.fold_left stmt (L.builder_at_end context then_bb) then_stmts)
          (L.build_br merge_bb);

        let else_bb = L.append_block context "else" the_function in
        add_terminal (
          List.fold_left stmt (L.builder_at_end context else_bb) else_stmts)
          (L.build_br merge_bb);

        ignore (L.build_cond_br bool_val then_bb else_bb builder);
        L.builder_at_end context merge_bb
    | A.While (predicate, body) ->

        let pred_bb = L.append_block context "while" the_function in
        ignore (L.build_br pred_bb builder);

        let body_bb = L.append_block context "while_body" the_function in
        add_terminal (
          List.fold_left stmt (L.builder_at_end context body_bb) body)
          (L.build_br pred_bb);

        let pred_builder = L.builder_at_end context pred_bb in
          let pred_struct = expr pred_builder predicate in
          ignore( check_struct_type pred_struct num_type pred_builder );
          let bool_val = is_true pred_struct pred_builder in

        let merge_bb = L.append_block context "merge" the_function in
        ignore (L.build_cond_br bool_val body_bb merge_bb pred_builder);
        L.builder_at_end context merge_bb
    | A.Foreach (x, e, body) -> let e' = expr builder e in
        let merge_bb = L.append_block context "merge_for_each" the_function in

        let bad_type_bb = L.append_block context "bad_type" the_function in
        let bad_type_builder = L.builder_at_end context bad_type_bb in
        ignore (L.build_call printf_func [| left_type_str |] "printf"
          bad_type_builder);
        ignore (L.build_call abort_func [||] "" bad_type_builder);
        ignore (L.build_br merge_bb bad_type_builder);

        let list_bb = L.append_block context "for_each_list" the_function in
        let map_bb = L.append_block context "for_each_map" the_function in

        let list_builder = L.builder_at_end context list_bb in
        build_for_each_list x e body list_builder;
        ignore (L.build_br merge_bb list_builder);

        let map_builder = L.builder_at_end context map_bb in
        build_for_each_map x e body map_builder;
        ignore (L.build_br merge_bb map_builder);

        let e_type = valid_struct_index e' builder in
        let switch = L.build_switch e_type bad_type_bb 2 builder in
          L.add_case switch two list_bb;
          L.add_case switch three map_bb;

        L.builder_at_end context merge_bb
    | A.Forkv (k, v, e, body) -> let e' = expr builder e in
        let merge_bb = L.append_block context "merge_for_kv" the_function in

        let bad_type_bb = L.append_block context "bad_type" the_function in
        let bad_type_builder = L.builder_at_end context bad_type_bb in
        ignore (L.build_call printf_func [| left_type_str |] "printf"
          bad_type_builder);
        ignore (L.build_call abort_func [||] "" bad_type_builder);
        ignore (L.build_br merge_bb bad_type_builder);

        let list_bb = L.append_block context "for_kv_list" the_function in
        let map_bb = L.append_block context "for_kv_map" the_function in

        let list_builder = L.builder_at_end context list_bb in
        build_for_kv_list k v e body list_builder;
        ignore (L.build_br merge_bb list_builder);

        let map_builder = L.builder_at_end context map_bb in
        build_for_kv_map k v e body map_builder;
        ignore (L.build_br merge_bb map_builder);

        let e_type = valid_struct_index e' builder in
        let switch = L.build_switch e_type bad_type_bb 2 builder in
          L.add_case switch two list_bb;
          L.add_case switch three map_bb;

        L.builder_at_end context merge_bb
      (* Do nothing. This happens if the statement is empty. *)
    | A.Nothing -> builder
    in

    (* Build the IR for the function's statements *)
    let builder = List.fold_left stmt builder fdecl.A.body in

    (* Point the variable memory malloction block to the statements block *)
    ignore (L.build_br stmts_bb malloc_builder);

    (* Return a null struct pointer from the function if we did not explicitly
       return before (except for the main function, which returns void) *)
    add_terminal builder (match fdecl.A.fname with
        "main" -> L.build_ret_void
      | _ -> L.build_ret (L.const_pointer_null (pointer_t struct_t)))
    in

  List.iter build_function_body functions;
  the_module

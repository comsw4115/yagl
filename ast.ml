(* Abstract Syntax Tree and functions for printing it *)

type eqop = Equal | Neq

type op = Add | Sub | Mult | Div | Mod | Less | Leq | Greater |
          Geq | And | Or

type uop = Neg | Not

type expr =
    Num of float
  | Inf of float
  | True of float
  | False of float
  | Id of string
  | String of string
  | Eqop of expr * eqop * expr
  | Binop of expr * op * expr
  | Unop of uop * expr
  | Assign of expr * expr
  | Call of string * expr list
  | Typeof of expr
  | Null
  | Graph
  | Digraph
  | List of expr list
  | Set of expr list
  | Map of (string * expr) list
  | ListOrMapAccess of expr * expr
  | MapStringAccess of expr * string
  | Noexpr

type stmt =
  | Expr of expr
  | Return of expr
  | If of expr * (stmt list) * (stmt list)
  | Foreach of expr * expr * (stmt list)
  | Forkv of expr * expr * expr * (stmt list)
  | While of expr * (stmt list)
  | Break
  | Continue
  | Nothing

type func_decl = {
    fname : string;
    formals : string list;
    body : stmt list;
  }

type program = func_decl list


(* Pretty-printing functions *)

let string_of_op = function
  Add -> "+"
| Sub -> "-"
| Mult -> "*"
| Div -> "/"
| Mod -> "%"
| Less -> "<"
| Leq -> "<="
| Greater -> ">"
| Geq -> ">="
| And -> "and"
| Or -> "or"

let string_of_eqop = function
Equal -> "=="
| Neq -> "!="
let string_of_uop = function
  Neg -> "-"
| Not -> "not "

let rec string_of_expr = function
  Num(n) -> string_of_float n
| Inf(_) -> "INF"
| True(_) -> "true"
| False(_) -> "false"
| Id(s) -> s
| String(s) -> s
| Binop(e1, o, e2) ->
    string_of_expr e1 ^ " " ^ string_of_op o ^ " " ^ string_of_expr e2
| Unop(o, e) -> string_of_uop o ^ string_of_expr e
| Assign(v, e) -> string_of_expr v ^ " = " ^ string_of_expr e
| Call(f, l) -> f ^ "( " ^ String.concat ", " (List.map string_of_expr l) ^ " )"
| Typeof( e ) -> "typeOf" ^ "( " ^ string_of_expr e ^ " )"
| Null -> "NULL"
| Graph -> "graph"
| Digraph -> "digraph"
| List(l) -> "[ " ^ String.concat ", " (List.map string_of_expr l) ^ " ]"
| Set(s) -> "{ " ^ String.concat ", " (List.map string_of_expr s) ^ " }"
| Map(m) -> "{| " ^
    String.concat ", "
      (List.map (fun t -> fst t ^ " := " ^ string_of_expr (snd t)) m) ^
    " |}"
| ListOrMapAccess(l, i) -> string_of_expr l ^ "[" ^ string_of_expr i ^ "]"
| MapStringAccess(m, s) -> string_of_expr m ^ "." ^ s
| Noexpr -> ""

let rec string_of_stmt = function
  Expr(expr) -> string_of_expr expr ^ "\n"
| Return(expr) -> "return " ^ string_of_expr expr ^ "\n"
| If(e, sl, []) -> "if ( " ^ string_of_expr e ^ " )\n{\n" ^
    String.concat "" (List.map string_of_stmt sl) ^ "}\n"
| If(e, sl1, sl2) -> "if ( " ^ string_of_expr e ^ " )\n{\n" ^
    String.concat "" (List.map string_of_stmt sl1) ^ "}\nelse\n{\n" ^
    String.concat "" (List.map string_of_stmt sl2) ^ "}\n"
| Foreach(e1, e2, sl) -> "forEach ( " ^
    string_of_expr e1 ^ ", " ^ string_of_expr e2 ^ " )\n{\n" ^
    String.concat "" (List.map string_of_stmt sl) ^ "}\n"
| Forkv(e1, e2, e3, sl) -> "forComponentValue ( " ^
    string_of_expr e1 ^ ", " ^ string_of_expr e2 ^ string_of_expr e3 ^
    " )\n{\n" ^ String.concat "" (List.map string_of_stmt sl) ^ "}\n"
| While(e, sl) -> "forEach ( " ^ string_of_expr e ^ " )\n{\n" ^
    String.concat "" (List.map string_of_stmt sl) ^ "}\n"
| Break -> "break\n"
| Continue -> "continue\n"

let string_of_fdecl fdecl =
  fdecl.fname ^ "(" ^ String.concat ", " fdecl.formals ^ ")\n{\n" ^
  String.concat "" (List.map string_of_stmt fdecl.body) ^ "}\n"

let string_of_program (funcs) =
  String.concat "\n\n" (List.map string_of_fdecl funcs)

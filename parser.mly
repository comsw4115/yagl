/* Ocamlyacc parser for YAGL */

%{
open Ast
%}

%token LPAREN RPAREN LBRACE RBRACE COMMA DOT LMAP RMAP LBRACKET RBRACKET QUOTE
%token NEWLINE
%token PLUS MINUS TIMES DIVIDE MOD ASSIGN NOT MAPASSIGN
%token EQ NEQ LT LEQ GT GEQ AND OR INF
%token RETURN IF ELSE FOREACH FORKV WHILE NULL DEF
%token GRAPH DIGRAPH
%token TYPEOF
%token <float> NUM
%token <string> ID
%token <string> STRING
%token EOF

%nonassoc NOELSE
%nonassoc ELSE
%right ASSIGN
%left OR
%left AND
%left EQ NEQ
%left LT GT LEQ GEQ
%left PLUS MINUS
%left TIMES DIVIDE MOD
%right NOT
%nonassoc NEG
%left DOT LBRACKET RBRACKET
%left RPAREN

%start program
%type <Ast.program> program

%%

program:
  NEWLINE program { $2 }
| decls EOF { $1 }

decls:
  /* nothing */ { [] }
| decls fdecl { $2 :: $1 }

arb_newline:
  /* nothing */ { () }
| NEWLINE arb_newline { () }

fdecl:
  DEF ID LPAREN formals_opt RPAREN LBRACE NEWLINE stmt_list RBRACE NEWLINE
  arb_newline
    { { fname = $2; formals = $4; body = List.rev $8 } }

formals_opt:
  /* nothing */ { [] }
| formal_list { List.rev $1 }

formal_list:
  ID { [$1] }
| formal_list COMMA ID { $3 :: $1 }

stmt_list:
  /* nothing */  { [] }
| stmt_list stmt { $2 :: $1 }

stmt:
  expr NEWLINE { Expr $1 }
| RETURN LPAREN RPAREN NEWLINE { Return Noexpr }
| RETURN LPAREN expr RPAREN NEWLINE { Return $3 }
| IF LPAREN expr RPAREN LBRACE NEWLINE stmt_list RBRACE NEWLINE %prec NOELSE
    { If($3, List.rev $7, []) }
| IF LPAREN expr RPAREN LBRACE NEWLINE stmt_list RBRACE NEWLINE ELSE LBRACE
  stmt_list RBRACE NEWLINE
    { If($3, List.rev $7, List.rev $12) }
| WHILE LPAREN expr RPAREN LBRACE NEWLINE stmt_list RBRACE NEWLINE
    { While($3, List.rev $7) }
| FOREACH LPAREN expr COMMA expr RPAREN LBRACE NEWLINE stmt_list RBRACE NEWLINE
    { Foreach($3, $5, List.rev $9) }
| FORKV LPAREN expr COMMA expr COMMA expr RPAREN LBRACE NEWLINE stmt_list RBRACE
  NEWLINE
    { Forkv($3, $5, $7, List.rev $11) }

| NEWLINE { Nothing }

expr:
| NUM { Num($1) }
| STRING { String($1) }
| LBRACKET actuals_opt RBRACKET { List($2) }
| LBRACE actuals_opt RBRACE { Set($2) }
| LMAP map_opt RMAP { Map($2) }
| GRAPH { Graph }
| DIGRAPH { Digraph }
| NULL { Null }
| TYPEOF LPAREN expr RPAREN { Typeof( $3 ) }
| expr PLUS expr { Binop($1, Add, $3) }
| expr MINUS expr { Binop($1, Sub, $3) }
| expr TIMES expr { Binop($1, Mult, $3) }
| expr DIVIDE expr { Binop($1, Div, $3) }
| expr MOD expr { Binop($1, Mod, $3) }
| expr EQ     expr { Eqop($1, Equal, $3) }
| expr NEQ    expr { Eqop($1, Neq,   $3) }
| expr LT     expr { Binop($1, Less,  $3) }
| expr LEQ    expr { Binop($1, Leq,   $3) }
| expr GT     expr { Binop($1, Greater, $3) }
| expr GEQ    expr { Binop($1, Geq,   $3) }
| expr AND    expr { Binop($1, And,   $3) }
| expr OR     expr { Binop($1, Or,    $3) }
| MINUS expr %prec NEG { Unop(Neg, $2) }
| NOT expr { Unop(Not, $2) }
| expr ASSIGN expr { Assign($1, $3) }
| ID LPAREN actuals_opt RPAREN { Call($1, $3) }
| expr LBRACKET expr RBRACKET { ListOrMapAccess($1, $3) }
/* NB: even though this is string access, we need to parse it as an ID because
   strings written with this syntax don't have quote marks around them. */
| expr DOT ID { MapStringAccess($1, $3) }
| ID { Id($1) }
| LPAREN expr RPAREN { $2 }

map_opt:
  /* nothing */ { [] }
| map_list { List.rev $1 }

map_list:
  STRING MAPASSIGN expr { [($1, $3)] }
| map_list COMMA STRING MAPASSIGN expr { ($3, $5) :: $1 }

actuals_opt:
  /* nothing */ { [] }
| actuals_list { List.rev $1 }

actuals_list:
  expr { [$1] }
| actuals_list COMMA expr { $3 :: $1 }

def foo(i) {
  /* Should reassign the formal i */
  i = 42
  print(i + i)
}

def main() {
  foo(true)
}

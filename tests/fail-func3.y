
def bar() {
}

def remove() {
} /* Should not be able to define remove */

def baz() {
}

def main() {
  print( 42 )
}

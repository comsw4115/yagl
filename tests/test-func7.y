def foo( c ) {
  a = c + 42
}

def main() {
  a = 0
  foo(100)
  print(a)
}

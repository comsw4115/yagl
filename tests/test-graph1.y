def main() {
  G = graph
  addV(G,'v1')
  addV(G,'v2')
  i = 1
  forKeyValue( label, v, v(G) ){
    v.cap = i
    i = i + 1
  }
  addV(G,'v3')
  print( v(G).v1.cap )
  print( v(G).v2.cap )
  print( v(G).v3.cap )
}

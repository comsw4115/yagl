def foo() {
}

def bar() {
}

def baz() {
}

def bar() {
} /* Error: duplicate function bar */

def main() {
  print( 42 )
}

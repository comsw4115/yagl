def cond(b) {
  if(b){
    x = 42
  }
  else {
    x = 17
  }
  return( x )
}

def zero(){
  return( 0 )
}

def main() {
 print(cond(10))
 print(cond(10-10))
 print(cond(zero()-1))
 print(cond(zero()))
}

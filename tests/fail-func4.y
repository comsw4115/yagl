
def bar() {
}

def print() {
} /* Should not be able to define print */

def baz() {
}

def main() {
  print( 42 )
}

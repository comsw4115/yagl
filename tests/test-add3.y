def add(x, y) {
  return( x + y )
}

def main() {
  print( add( 'Hello', 'World' ) )
  print( add( 40, 2 ) )
}

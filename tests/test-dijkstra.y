/* Given a list of vertices, return the index of the vertex with the lowest
   value in the 'dist' attribute */
def minDistU( vertices ){
   minVertex = 0
   qCounter = 0
   minDistSoFar = INF
   forKeyValue( i, v, vertices ){
      thisDist = v.dist
      if( thisDist < minDistSoFar ) {
         minDistSoFar = thisDist
         minVertex = qCounter
      }
      qCounter = qCounter + 1
   }
   return( minVertex )
}

/* Run Dijkstra's algorithm from the source vertex to every other vertex */
def dijkstras( G, source ){
  Q = []

  forKeyValue( label, v, v( G ) ){
      v.dist = INF
      v.prev = NULL
      append( v(G)[ label ], Q )
  }

  v(G)[source].dist = 0

  while( not isEmpty( Q ) ){
    u = remove( minDistU(Q), Q )

    forKeyValue( i, v, adj( G, u ) ) {
      alt = u.dist + e(G)[ edgeLabel( u, v ) ].length
      if ( alt < v.dist ) {
        v.dist = alt
        v.prev = u
      }
    }
  }
}

def main() {
  G = graph

  /* Construct the graph */
  e1 = addE( G, 'a', 'b' )
  e2 = addE( G, 'a', 'c' )
  e3 = addE( G, 'a', 'f' )
  e4 = addE( G, 'b', 'c' )
  e5 = addE( G, 'b', 'd' )
  e6 = addE( G, 'c', 'f' )
  e7 = addE( G, 'c', 'd' )
  e8 = addE( G, 'd', 'e' )
  e9 = addE( G, 'f', 'e' )
  e1.length = 7
  e2.length = 9
  e3.length = 14
  e4.length = 10
  e5.length = 15
  e6.length = 2
  e7.length = 11
  e8.length = 6
  e9.length = 9

  dijkstras( G, 'a' )

  print( 'Shortest distances' )
  print( 'a to a' )
  print( v( G ).a.dist )
  print( 'a to b' )
  print( v( G ).b.dist )
  print( 'a to c' )
  print( v( G ).c.dist )
  print( 'a to d' )
  print( v( G ).d.dist )
  print( 'a to e' )
  print( v( G ).e.dist )
  print( 'a to f' )
  print( v( G ).f.dist )

  /* Get the shortest path from vertex a to vertex e */
  u = v( G ).e
  path = [ 'e' ]
  while ( not isEqual( u.prev, NULL ) ) {
    u = v( G )[ u.prev.label ]
    push( u.label, path )
  }
  print( 'Shortest path from vertex a to vertex e' )
  while( size( path ) > 0 ) {
    print( pop( path ) )
  }
}

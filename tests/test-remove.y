def main() {
  a = [4, 5, 6]
  remove( 1, a )
  forKeyValue( k, v, a ) {
    print( v )
  }

  b = {| 'key1' := 'hello', 'key2' := 'world' |}
  remove( 'key2', b )
  print( b.key1 )
  print( b.key2 )
}

def main() {
  a = {| 'key1' := 1, 'key2' := 'two' |}
  a.key3 = ['three']
  a.key4 = {| 'key5' := '5' |}
  a.key4.key5 = '6'

  print( a.key1 )
  print( a.key2 )
  print( a.key3[0] )
  print( a['key4'].key5 )
}


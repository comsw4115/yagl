def main() {
   a = {| 'key1' := 1,'key2' := 2,'key3' := 3 |}
   forKeyValue(k,v,a){
    v = k
   }
   forKeyValue(k,v,a){
    print( v )
   }
}

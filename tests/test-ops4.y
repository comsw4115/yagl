def main() {
  print( 'a' == 1 )
  print( 'a' != 1 )
  print( 'a' == NULL )
  print( NULL == NULL )
  print( 'a' == 'a' )
  print( 'a' != 'a' )
  print( 'foo' != 'bar' )
  print( 'foo' == 'bar' )
}

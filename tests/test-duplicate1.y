def addCap( e ) {
  e.cap = '10'
}

def main() {
  a = {||}
  b = deepCopy(a)
  b.cap = '0'
  addCap( a )
  print( a.cap )
  print( b.cap )
}

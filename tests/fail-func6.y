def foo(a,b) {
}

def main() {
  foo(42, true)
  foo(42) /* Wrong number of arguments */
}

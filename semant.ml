(* Semantic checking for the yagl compiler *)

open Ast
open String
module StringMap = Map.Make(String)

(* Semantic checking of a program. Returns void if successful,
   throws an exception if something is wrong.

   Check each global variable, then check each function *)

let check (functions) =

  (* Raise an exception if the given list has a duplicate *)
  let report_duplicate exceptf list =
    let rec helper = function
	n1 :: n2 :: _ when n1 = n2 -> (); raise (Failure (exceptf n1))
      | _ :: t -> helper t
      | [] -> ()
    in helper (List.sort compare list)
  in

(*   let check_empty execptf = function
    _ -> raise (Failure (execptf) )
  in *)

  (* Raise an exception of the given rvalue type cannot be assigned to
     the given lvalue type *)
(*   let check_assign lvaluet rvaluet err =
     if lvaluet == rvaluet then lvaluet else raise err
  in *)

  (**** Checking Functions ****)

  if List.mem "print" (List.map (fun fd -> fd.fname) functions)
  then raise (Failure ("function print may not be defined")) else ();

  if List.mem "remove" (List.map (fun fd -> fd.fname) functions)
  then raise (Failure ("function remove may not be defined")) else ();

  if List.mem "append" (List.map (fun fd -> fd.fname) functions)
  then raise (Failure ("function remove may not be defined")) else ();

  if List.mem "v" (List.map (fun fd -> fd.fname) functions)
  then raise (Failure ("function remove may not be defined")) else ();

  if List.mem "e" (List.map (fun fd -> fd.fname) functions)
  then raise (Failure ("function remove may not be defined")) else ();

  if List.mem "addV" (List.map (fun fd -> fd.fname) functions)
  then raise (Failure ("function remove may not be defined")) else ();

  if List.mem "addE" (List.map (fun fd -> fd.fname) functions)
  then raise (Failure ("function remove may not be defined")) else ();

  report_duplicate (fun n -> "duplicate function " ^ n)
    (List.map (fun fd -> fd.fname) functions);

  (* Function declaration for a named function *)
  let built_in_decls =
   (StringMap.add "v" { fname = "v"; formals = [("G")]; body = []}
   (StringMap.add "addV" { fname = "addV"; formals = [("G");("v")]; body = []}
   (StringMap.add "e" { fname = "e"; formals = [("G")]; body = []}
   (StringMap.add "addE" { fname = "addE"; formals = [("G");("v1");("v2")];
     body = []}
   (StringMap.add "print" { fname = "print"; formals = [("x")]; body = [] }
   (StringMap.add "remove" { fname = "remove"; formals = [("i");("ls")];
     body = [] }
   (StringMap.singleton "append" { fname = "append"; formals = [("l");("e")];
     body = [] }
   )))))))
  in

  let function_decls = List.fold_left (fun m fd -> StringMap.add fd.fname fd m)
                         built_in_decls functions
  in

  let function_decl s = try StringMap.find s function_decls
       with Not_found -> raise (Failure ("Unrecognized function " ^ s))
  in

  let _ = function_decl "main" in (* Ensure "main" is defined *)

  let check_function func =

    report_duplicate (fun n -> "duplicate formal " ^ n ^ " in " ^ func.fname)
      func.formals;

    (* Returns Boolean coresponding to if is valid lvalue in assignment
       Raises if invalid assignment *)
    let rec expr = function
        Num(_) -> false
      | True(_) -> false
      | False(_) -> false
      | Id(x) ->  true
      | String(_) -> false
      (* Binary/Equality operators are never valid lvalues *)
      | Binop(e1, _,e2) -> ignore( expr e1 ); ignore( expr e2 ); false
      | Eqop(e1, _,e2) -> ignore( expr e1 ); ignore( expr e2 ); false
      | Unop( _, e ) -> ignore( expr e ); false
      | Assign( e1, e2 ) -> let t1 = expr e1 in
          if not t1 then raise
            (Failure ("\n Illegal assignment!\n Can only assign to variables." ^
              " Cannot assign to litteral declarations, results of calls, or" ^
              " expressions\n Failed Expression: " ^
              string_of_expr e1 ^ "=" ^ string_of_expr e2 ^ "\n") )
          else expr e2
      | Call(fname, actuals) as call -> let fd = function_decl fname in
         if List.length actuals != List.length fd.formals then
           raise (Failure ("expecting " ^ string_of_int
             (List.length fd.formals) ^ " arguments in " ^ string_of_expr call))
         else
           ignore( List.map expr actuals );
           false;
      | Null -> false;
      | Graph -> false;
      | Digraph -> false;
      | List(exprs) -> ignore( List.map expr exprs ); false;
      | Set(exprs) -> ignore( List.map expr exprs ); false;
      | Map(pairs) -> ignore( report_duplicate (fun n -> "duplicate key " ^ n)
                        (List.map fst pairs) );
                      ignore( List.map expr (List.map snd pairs) );
                      false;
      | ListOrMapAccess(e1, e2) -> ignore (expr e1); ignore (expr e2); true;
      | MapStringAccess(_, _) -> true
      | Noexpr -> false
      | Inf(_) -> false
      | Typeof(_) -> false
    in

    (* Verify a statement or throw an exception *)
    let rec check_statement_list = function
           [Return _ as s] -> stmt s
         | Return _ :: _ -> raise (Failure "nothing may follow a return")
         | s :: ss -> stmt s ; check_statement_list ss
         | [] -> ()
    and

     stmt = function
          Expr e -> ignore( expr e )
        | Return e -> if func.fname = "main" then
                        raise (Failure ("You may not return in main function" ))
                      else ignore( expr e )
        | If( e, stmts1, stmts2 ) ->
            ignore (expr e);
            check_statement_list stmts1; check_statement_list stmts2;
        | Foreach(x, e, stmts ) -> ignore(expr e);
             (match x with
            Id(_) -> check_statement_list stmts;
           | _ -> raise (Failure  ("The first arg of ForEach must be an Id" ) )
                               )
        | Forkv(x, y, e, stmts ) -> ignore (expr e);
             (match x,y with
            Id(_),Id(_) -> check_statement_list stmts;
           | _ -> raise
            (Failure("The first two args of ForComponentValue must be an Id" ) )
                               )
        | While( e, stmts ) -> ignore (expr e); check_statement_list stmts;
        | Nothing -> ()
    in
        check_statement_list func.body
  in

  List.iter check_function functions

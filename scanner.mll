(* Ocamllex scanner for YAGL *)

{ open Parser }

rule token = parse
  [' ' '\t' '\r'] { token lexbuf } (* Whitespace *)
| '\n' { NEWLINE }
| "/*" { m_comment lexbuf }
| "//" { s_comment lexbuf }
| "'" { QUOTE }
| '(' { LPAREN }
| ')' { RPAREN }
| '{' { LBRACE }
| '}' { RBRACE }
| "{|" { LMAP }
| "|}" { RMAP }
| '[' { LBRACKET }
| ']' { RBRACKET }
| ',' { COMMA }
| '.' { DOT }
| '+' { PLUS }
| '-' { MINUS }
| '*' { TIMES }
| '/' { DIVIDE }
| '%' { MOD }
| '=' { ASSIGN }
| ":=" { MAPASSIGN }
| "==" { EQ }
| "!=" { NEQ }
| '<' { LT }
| "<=" { LEQ }
| '>' { GT }
| ">=" { GEQ }
| "INF" { NUM(infinity) }
| "and" { AND }
| "or" { OR }
| "not" { NOT }
| "if" { IF }
| "else" { ELSE }
| "forKey" { FOREACH }
| "forKeyValue" { FORKV }
| "while" { WHILE }
| "NULL" { NULL }
| "true" { NUM(1.) }
| "false" { NUM(0.) }
| "def" { DEF }
| "return" { RETURN }
| "graph" { GRAPH }
| "digraph" { DIGRAPH }
| "typeOf" { TYPEOF }
| ['0'-'9']*'.'?['0'-'9']+ as lxm { NUM(float_of_string lxm) }
| ['0'-'9']+'.'?['0'-'9']* as lxm { NUM(float_of_string lxm) }
| ['a'-'z' 'A'-'Z']['a'-'z' 'A'-'Z' '0'-'9' '~']* as lxm { ID(lxm) }
| "'"['a'-'z' 'A'-'Z' '0'-'9' ' ' '~']*"'" as lxm {
    STRING(String.sub lxm 1 (String.length lxm - 2)) }
| eof { EOF }
| _ as char { raise (Failure("The following character is an illegal YAGL character '" ^
              Char.escaped char ^ "'")) }

and m_comment = parse
  "*/" { token lexbuf }
| _ { m_comment lexbuf }

and s_comment = parse
  '\n' { token lexbuf }
| _ { s_comment lexbuf }
